from django.core.management.base import BaseCommand, CommandError
from django.apps import apps
import re as regex
from django_seed import Seed


class Command(BaseCommand):

    def handle(self, *args, **options):
        model = options.get('model')
        app = options.get('app')
        records = options.get('records') or 1
        related = options.get('related') or None

        pattern = r'\w+'
        validate = regex.compile(pattern)
        if validate.fullmatch(model) and validate.fullmatch(app):
            app_model = apps.get_model(app, model)
            seeder = Seed.seeder()
            if related:
                related = related.strip(' ,')
                bases = related.split(',')
                for entry in bases:
                    x, y = entry.strip().split('.')
                    _model = apps.get_model(x, y)
                    seeder.add_entity(_model, records)
            seeder.add_entity(app_model, records)
            generated_pks = seeder.execute()
            if generated_pks:
                self.stdout.write(self.style.SUCCESS('Generated records' % generated_pks))
            else:
                raise CommandError('Unable to generate records for named model')

    def add_arguments(self, parser):
        parser.add_argument('--model', type=str, help='Name of the target model for which '
                                                      'data is to be generated')
        parser.add_argument('--records', type=int, help='Number of records to generate')
        parser.add_argument('--app', type=str, help='Name of the target application for the '
                                                    'listed model')
        parser.add_argument('--related', type=str, help='List related models as '
                                                        '<app>.<model> separated by comma(,)')
