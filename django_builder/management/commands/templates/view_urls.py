from string import Template

view_url_template = Template(
"""
urlpatterns += (
    # urls for $model
    path('$model_url/', views.$model_list_view.as_view(), name='$model_url-list'),
    path('$model_url/create/', views.$model_create_view.as_view(), name='$model_url-create'),
    path('$model_url/detail/<$capture_type:$pk_field>/', views.$model_detail_view.as_view(), name='$model_url-detail'),
    path('$model_url/update/<$capture_type:$pk_field>/', views.$model_update_view.as_view(), name='$model_url-update'),
    path('$model_url/delete/<$capture_type:$pk_field>/', views.$model_delete_view.as_view(), name='$model_url-delete')    
)

"""
)

view_url_import = Template(
"""
from django.urls import path
from $app import views

app_name = '$app' 

urlpatterns = []
"""
)
