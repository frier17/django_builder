"""
Build the serializer from a given model class or name
@todo: define template in text files
"""
import keyword
import os
import re as regex
from typing import List, Tuple

import stringcase
from django.core.management.base import BaseCommand, CommandError
from rest_framework import permissions as perm

from .templates.api import api_import, api_template
from .templates.forms import form_template, form_import
from .templates.serializer import serializer_import, serializer_template
from .templates.template import instance_form_template, list_template, confirm_delete_template, detail_template
from .templates.urls import url_template, url_import
from .templates.view_urls import view_url_template, view_url_import
from .templates.views import view_template, view_import


def _build_serializer(**kwargs):
    _model = kwargs.get('model')
    _app = kwargs.get('app') or '.'
    _fields = kwargs.get('fields')
    _serializer = kwargs.get('serializer')
    _add_import = kwargs.get('add_import')

    pattern = r'\w+'
    validate = regex.compile(pattern, regex.IGNORECASE)
    if not all([validate.fullmatch(x) and not keyword.iskeyword(x) for x in
                (_model, _serializer, _app)]):
        raise CommandError('Invalid name provided as model, serializer or application')

    if isinstance(_fields, List) or isinstance(_fields, Tuple):
        _f = ["'%s'" % x for x in _fields if isinstance(x, str) and not keyword.iskeyword(x)]
        fields_str = ','.join(_f)
        _fields = '[%s]' % fields_str
    elif isinstance(_fields, str):
        if _fields != '__all__':
            # validate _fields matches pattern: [a, b, c] or (a, b, c)
            pattern = r'[\[\(](["\']\w+["\']\s*\,\s*)+["\']\w*["\'][\]\)]'
            validate = regex.compile(pattern, regex.IGNORECASE)
            if not validate.fullmatch(_fields):
                raise CommandError('Invalid field list provided as string')

    if _fields == '__all__':
        s_template = serializer_template.substitute(model=_model, serializer=_serializer, fields="'__all__'")
    else:
        s_template = serializer_template.substitute(model=_model, serializer=_serializer, fields=_fields)
    s_import = serializer_import.substitute(app=_app)

    if _add_import:
        return s_import + s_template
    else:
        return s_template


def _build_api(**kwargs):
    _model = kwargs.get('model')
    _viewset = kwargs.get('viewset')
    _app = kwargs.get('app')
    _manager = kwargs.get('manager')
    _permissions = kwargs.get('permissions')
    _serializer = kwargs.get('serializer')
    _add_import = kwargs.get('add_import')

    pattern = r'\w+'
    is_valid = regex.compile(pattern, regex.IGNORECASE)
    if not all([is_valid.fullmatch(x) and not keyword.iskeyword(x) for x in
                (_model, _manager, _app, _serializer, _viewset)]):
        raise CommandError('Invalid name provided as parameter for generator.'
                           'Name format should be compliant with Python guideline')

    if isinstance(_permissions, List) or isinstance(_permissions, Tuple):
        _f = ["%s" % x for x in _permissions if isinstance(x, str)
              and not keyword.iskeyword(x)]
        _p = ["%s" % x.__name__
              for x in _permissions if issubclass(x, perm.BasePermission)]
        permission_str = ''
        if _f:
            permission_str = ','.join(_f)
        elif _p:
            permission_str = ','.join(_p)
        _permissions = '[%s]' % permission_str
    elif isinstance(_permissions, str):
        # validate _fields matches pattern: [a, b, c] or (a, b, c)
        pattern = r'[\[\(](\w+(\.\w+)*\s*\,\s*)+\w*[\,\]\)]'
        validate = regex.compile(pattern, regex.IGNORECASE)
        if not validate.match(_permissions):
            raise CommandError('Invalid permission list provided as string')

    a_template = api_template.substitute(model=_model, model_manager=_manager, serializer=_serializer, viewset=_viewset,
                                         permissions=_permissions)
    if _add_import:
        a_import = api_import.substitute(app=_app)
        return a_import + a_template
    else:
        return a_template


def _build_api_urls(**kwargs):
    _app = kwargs.get('app')
    _model = kwargs.get('model')
    _router = kwargs.get('router') or 'DefaultRouter'
    _add_import = kwargs.get('add_import') or False

    pattern = r'\w+'
    validate = regex.compile(pattern)
    if not all([x for x in (_app, _model, _router) if validate.fullmatch(x)]):
        raise Exception('Invalid permission list provided as string')

    _url_template = url_template.substitue(model=_model, end_point=stringcase.spinalcase(_model), app=_app,
                                           router=_router)

    if _add_import:
        _url_import = url_import.substitute(app=_app)
        return _url_import + _url_template
    else:
        return _url_template


def _build_forms(**kwargs):
    _model = kwargs.get('model')
    _fields = kwargs.get('fields')
    _default_model_form = kwargs.get('model_form') or 'ModelForm'
    _app = kwargs.get('app')
    _add_import = kwargs.get('add_import')
    try:
        from django.db import models
        from django.apps import apps
        _instance = apps.get_model(_app, _model, require_ready=False)
        # validate model and app name
        pattern = r'\w+'
        is_valid = regex.compile(pattern, regex.IGNORECASE)
        if not all([is_valid.fullmatch(x) and not keyword.iskeyword(x) for x in
                    (_model, _default_model_form, _app)]):
            raise CommandError('Invalid name provided as parameter for generator.'
                               'Name format should be compliant with Python guideline')

        all_fields = []
        if issubclass(_instance, models.Model):
            all_fields = [str(x.name) for x in _instance._meta.get_fields()]
        if isinstance(_fields, (list, tuple)):
            _fields = [x for x in _fields if x in all_fields]
        elif isinstance(_fields, str):
            if _fields != '__all__':
                pattern = r'[\[\(](["\']\w+["\']\s*\,\s*)+["\']\w*["\'][\]\)]'
                validate = regex.compile(pattern, regex.IGNORECASE)
                if not validate.fullmatch(_fields):
                    raise CommandError('Invalid field list provided as string')
        else:
            _fields = '__all__'

        if kwargs.get('exclude'):
            _excluded_fields = ['slug', 'uuid', 'id', 'pk', 'is_shareable',
                                'is_public', 'created_at', 'last_modified_at', 'deleted_at',
                                'force_delete']
            _fields = [x for x in all_fields if x not in _excluded_fields]

        if isinstance(_fields, List) or isinstance(_fields, Tuple):
            _f = ["'%s'" % x for x in _fields if
                  isinstance(x, str) and not keyword.iskeyword(x)]
            fields_str = ','.join(_f)
            _fields = '[%s]' % fields_str

        if _fields == '__all__':
            f_template = form_template.substitute(
                model_form=f'{_model}Form',
                default_model_form=_default_model_form,
                model=_model,
                fields="'__all__'")
        else:
            f_template = form_template.substitute(model=_model, model_form=_default_model_form, fields=_fields)

        f_import = form_import.substitute(app=_app)

        if _add_import:
            return f_import + f_template
        else:
            return f_template

    except Exception:
        raise


def _build_template(**kwargs):
    _model = kwargs.get('model')
    _app = kwargs.get('app')
    _fields = kwargs.get('fields') or []
    _headings = kwargs.get('headings') or []
    _exclude = kwargs.get('exclude') or False
    _exclude_fields = ['slug', 'uuid', 'id', 'pk', 'is_shareable', 'is_public', 'deleted_at', 'force_delete'] \
        if _exclude else []

    def _make_model_list(**list_options):
        # Generate table structure for model list view
        mf = list_options.get('model_fields')
        _model_update_url = list_options.get('model_update_url')
        _model_detail_url = list_options.get('model_detail_url')
        _model_delete_url = list_options.get('model_delete_url')
        _model_create_url = list_options.get('model_create_url')

        commands = \
            f"""
<td>
    <ul class="list-inline">
        <li class="list-inline-item">
            <a href="{{% url '{model_detail_url}' object.slug %}}" class="cmd 
                cmd-detail" data-toggle="tooltip" title="View"><i class="fa fa-search-plus">&nbsp;</i>
            </a>
        </li>
        <li class="list-inline-item">
            <a href="{{% url '{model_update_url}' object.slug %}}" class="cmd cmd-edit" data-toggle="tooltip" 
title="Edit">
                <i class="fa fa-edit">&nbsp;</i>
            </a>
        </li>
        <li class="list-inline-item">
            <a href="{{% url '{model_delete_url}' object.slug %}}" class="cmd 
            cmd-delete" data-toggle="tooltip" title="Delete"><i class="fa fa-trash">&nbsp;</i>
            </a>
        </li>
    </ul>
</td>
"""

        template = ''
        for f in mf:
            start = "<td>{{object.%s}}</td>" % f
            template += start
        return template + commands

    def _make_list_heading(**list_options):
        mf = list_options.get('model_fields')
        mf = [stringcase.titlecase(str(x)) for x in mf]
        commands = '<th style="width: 80px">Actions</th>'
        template = ""
        for f in mf:
            start = "<th>%s</th>" % f
            template += start
        return template + commands

    def _make_model_detail(**detail_options):
        # Display all model fields as name-value pair
        mf = detail_options.get('model_fields')
        start = '<dt class="col-sm-3 py-3">{0}</dt><dd class="col-sm-9 py-3">{{{{ {1} }}}}</dd>'
        snippet = '<dl class="row">'
        for field in mf:
            snippet += start.format(stringcase.titlecase(str(field)), f'object.{field}')
        snippet += '</dl>'
        return snippet

    try:
        from django.db import models
        from django.apps import apps
        _instance = apps.get_model(_app, _model, require_ready=False)
        if issubclass(_instance, models.Model):
            # get all the fields of the model
            fields = [str(x.name) for x in _instance._meta.get_fields()]
            if _exclude:
                fields = list(set(fields) - set(_exclude_fields))
            if _fields:
                _fields = map(lambda x: x in fields, _fields)
            else:
                _fields = fields
            if not _headings:
                _headings = [x.replace('_', ' ').capitalize() for x in fields]
        model_list_url = f'{str(_app).lower()}:{stringcase.spinalcase(str(_model))}-list'
        model_detail_url = f'{str(_app).lower()}:{stringcase.spinalcase(_model)}-detail'
        model_delete_url = f'{str(_app).lower()}:{stringcase.spinalcase(_model)}-delete'
        model_update_url = f'{str(_app).lower()}:{stringcase.spinalcase(_model)}-update'
        model_create_url = f'{str(_app).lower()}:{stringcase.spinalcase(_model)}-create'
        # get the model fields and values as dict

        model_details = _make_model_detail(model_fields=_fields)
        list_records = _make_model_list(model_fields=_fields,
                                        model_detail_url=model_detail_url,
                                        model_update_url=model_update_url,
                                        model_delete_url=model_delete_url,
                                        model_create_url=model_create_url
                                        )
        list_heading = _make_list_heading(model_fields=_fields)
        _form_template = instance_form_template.substitute(model_list_url=model_list_url,
                                                           model=stringcase.titlecase(_model))

        _list_template = list_template.substitute(records=list_records, heading=list_heading,
                                                  model_create_url=model_create_url)
        _detail_template = detail_template.substitute(model=stringcase.titlecase(_model), model_list_url=model_list_url,
                                                      details=model_details, model_update_url=model_update_url,
                                                      model_delete_url=model_delete_url)
        _confirm_delete_template = confirm_delete_template.substitute(model=stringcase.titlecase(_model),
                                                                      model_list_url=model_list_url)
        return {
            'list': _list_template, 'detail': _detail_template, 'form': _form_template,
            'delete': _confirm_delete_template
        }

    except LookupError:
        # Model does not exits
        # raise CommandError('Model does not exist in specified application')
        raise
    except ValueError:
        raise
        # raise CommandError('Invalid application and model name provided')
    except AttributeError:
        raise CommandError('Attribute does not exist in model or application')
    except Exception:
        raise Exception


def _build_paginated_list(**kwargs):
    # Enable the generation of template with pagination
    return NotImplemented


def _build_view_urls(**kwargs):
    model_name = kwargs.get('model')
    _model = model_name
    _app = kwargs.get('app')
    _model_url = stringcase.spinalcase(str(_model))
    _model_lower = stringcase.snakecase(str(_model))
    _capture_type = kwargs.get('capture_type')
    _pk_field = kwargs.get('pk_field')
    _add_import = kwargs.get('add_import')

    pattern = r'\w+'
    is_valid = regex.compile(pattern, regex.IGNORECASE)
    if not all([is_valid.fullmatch(x) and not keyword.iskeyword(x) for x in
                (_model, _app)]):
        raise CommandError('Invalid name provided as parameter for generator.'
                           'Name format should be compliant with Python guideline')
    _url_template = view_url_template.substitute(
        model=_model,
        app=_app,
        model_url=_model_url,
        model_list_view=f'{model_name}ListView',
        model_create_view=f'{model_name}CreateView',
        model_detail_view=f'{model_name}DetailView',
        model_delete_view=f'{model_name}DeleteView',
        model_update_view=f'{model_name}UpdateView',
        model_lower=_model_lower,
        capture_type=_capture_type,
        pk_field=_pk_field,
    )
    _url_import = view_url_import.substitute(app=_app, model=_model)
    if _add_import:
        return _url_import + _url_template
    else:
        return _url_template


def _build_views(**kwargs):
    _app = str(kwargs.get('app'))
    model_name = kwargs.get('model')
    _model = model_name
    _add_import = kwargs.get('add_import')
    _app_lower = stringcase.snakecase(_app)
    _model_lower = stringcase.snakecase(_model) if not _model.isupper() else _model.lower()
    pattern = r'\w+'

    is_valid = regex.compile(pattern, regex.IGNORECASE)
    if not all([is_valid.fullmatch(x) and not keyword.iskeyword(x) for x in
                (_model, _app,)]):
        raise CommandError('Invalid name provided as parameter for generator.'
                           'Name format should be compliant with Python guideline')
    if not _model.isupper():
        _model = stringcase.spinalcase(_model).lower()

    _view_template = view_template.substitute(
        app=_app,
        model=model_name,
        model_url=_model,
        app_case=_app_lower,
        model_case=_model_lower,
        model_case_list=f'{_model_lower}_list',
        model_case_detail=f'{_model_lower}_detail',
        model_case_form=f'{_model_lower}_form',
        model_case_confirm_delete=f'{_model_lower}_confirm_delete',
        model_list_view=f'{model_name}ListView',
        model_create_view=f'{model_name}CreateView',
        model_detail_view=f'{model_name}DetailView',
        model_update_view=f'{model_name}UpdateView',
        model_delete_view=f'{model_name}DeleteView',
        model_form=f'{model_name}Form'
    )
    _view_import = view_import.substitute(app=_app)
    if _add_import:
        return _view_import + _view_template
    else:
        return _view_template


class Command(BaseCommand):
    help = 'Generate Django application serializer, api, and urls files which are ' \
           'stored in the specified django app'

    def handle(self, *args, **options):

        def write_file(application, make_file, output):
            _file = None
            can_write = False
            if '.html' in make_file:
                _file = os.path.join(os.getcwd(), application, make_file)
            else:
                _file = os.path.join(os.getcwd(), application, make_file + '.py')
            if os.path.isfile(_file):
                can_write = True
            else:
                try:
                    if make_file.endswith('.html'):
                        subdir, _, html = make_file.rpartition(os.path.sep)
                        subdir = os.path.join(os.getcwd(), subdir)
                        os.makedirs(subdir, exist_ok=True)
                        _file = os.path.join(subdir, html)
                        with open(_file, 'x'):
                            can_write = True
                    elif str(_file).endswith('.py'):
                        with open(_file, 'x'):
                            can_write = True
                except FileExistsError:
                    can_write = True
                except FileNotFoundError:
                    with open(_file, 'x'):
                        can_write = True
            if can_write:
                with open(_file, 'a') as fh:
                    fh.write(output)
                self.stdout.write(self.style.SUCCESS('Application written to %s' % _file))
            else:
                raise CommandError('An invalid file object was provided or IO error occurred')

        app = options.get('app')
        serializer = options.get('serializer') or 'ModelSerializer'
        fields = options.get('fields')
        model = options.get('model')
        viewset = options.get('viewset') or 'ModelViewSet'
        manager = options.get('manager') or 'objects'
        permissions = options.get('permissions') or [perm.IsAuthenticated]
        add_import = options.get('import')
        router = options.get('router') or 'DefaultRouter'
        make = options.get('make_file')
        capture_type = options.get('capture_type')
        exclude = options.get('exclude')
        pk_field = options.get('pk_field')
        model_form = options.get('model_form') or 'ModelForm'
        if make == 'serializers':
            out = _build_serializer(model=model, app=app, serializer=serializer, fields=fields,
                                    add_import=add_import)
            write_file(app, make, out)
        elif make == 'urls':
            out = _build_api_urls(app=app, model=model, router=router, add_import=add_import)
            write_file(app, make, out)
        elif make == 'api':
            out = _build_api(model=model, viewset=viewset, app=app, manager=manager,
                             permissions=permissions, serializer=serializer,
                             add_import=add_import)
            write_file(app, make, out)
        elif make == 'views':
            out = _build_views(app=app, model=model, add_import=add_import)
            write_file(app, make, out)
        elif make == 'forms':
            out = _build_forms(app=app, fields=fields, model=model, model_form=model_form,
                               add_import=add_import, exclude=exclude)
            write_file(app, make, out)
        elif make == 'views_url':
            out = _build_view_urls(app=app, model=model, add_import=add_import,
                                   capture_type=capture_type, pk_field=pk_field)
            write_file(app, 'urls', out)
        elif make == 'template':
            out = _build_template(app=app, model=model, exclude=exclude)
            write_file(app, f'{app}/templates/{app}/'
                            f'{stringcase.snakecase(str(model))}_list.html',
                       out.get('list'))
            write_file(app, f'{app}/templates/{app}/'
                            f'{stringcase.snakecase(str(model))}_detail.html',
                       out.get('detail'))
            write_file(app, f'{app}/templates/{app}/'
                            f'{stringcase.snakecase(str(model))}_form.html',
                       out.get('form'))
            write_file(app, f'{app}/templates/{app}/'
                            f'{stringcase.snakecase(str(model))}_confirm_delete.html',
                       out.get('delete'))
        else:
            # raise make file error
            raise CommandError('Make file specified is invalid. Allowed files are: '
                               'serializer, api, urls, views_url, etc. Use --help to see all '
                               'options ')

    def add_arguments(self, parser):
        parser.add_argument('-m', '--model', type=str, help='The model class name '
                                                            'provided as string without '
                                                            'quotes')
        parser.add_argument('--model_form', type=str,
                            help='The type of rest_framework serializer that will be extended '
                                 'by the application in '
                                 'generating forms for models'
                            )
        parser.add_argument('-a', '--app', type=str, help='The name of the target Django '
                                                          'application where the generated '
                                                          'files will be stored and '
                                                          'where the specified model was '
                                                          'defined')
        parser.add_argument('--fields', type=str, help='List of model fields for the model '
                                                       'serializer')
        parser.add_argument('--viewset', type=str, help='The type of rest_framework viewset '
                                                        'that will be extended by the '
                                                        'application. Example, ModelViewSet')
        parser.add_argument('--manager', type=str, help='The model field serving as default '
                                                        'Model Manager for the named model')
        parser.add_argument('--serializer', type=str,
                            help="The type of rest_framework serializer that will be extended "
                                 "by the "
                                 "application or the model serializer defined in application. "
                                 "Example, ModelSerializer")
        parser.add_argument('--permissions', type=str, help='List of permissions provided '
                                                            'as string')
        parser.add_argument('--router', type=str, help='The type of rest_framework router '
                                                       'that will be used for generating '
                                                       'urls. Example, DefaultRouter')
        parser.add_argument('--import', action='store_true', help='Flag to enable import of '
                                                                  'dependencies')
        parser.add_argument('--capture_type', type=str,
                            help='The data type to be captured in the Urlconf')
        parser.add_argument('--pk_field', type=str, help='The name of the view parameter '
                                                         'used for selection of object')
        parser.add_argument('--make', type=str, help='Name of the file to generate. The name '
                                                     'should be provided without the file '
                                                     'extension. '
                                                     'Example '
                                                     'api')
        parser.add_argument('make_file', type=str, help='Name of the file to generate. The '
                                                        'name '
                                                        'should be provided without the file '
                                                        'extension. '
                                                        'Example '
                                                        'api')
        parser.add_argument('--exclude', action='store_true',
                            help='Flag to exclude selected model fields from view. '
                                 'Current field list includes pk, id, uuid, slug, is_public, '
                                 'is_shareable')
