from string import Template

# generate the list template
list_template = Template(
"""
{% extends "base.html" %}
{% load static %}
{% block content %}
{% if object_list %}
    <div class="container my-5">
    <div class="row">
        <div class="col">    
            <table class="table table-striped table-bordered table-responsive table-sm data-table">
                <thead class="thead-dark">
                <tr>$heading</tr>
                </thead>
                <tbody>
                {% for object in object_list %}
                    <tr>
                        <!-- Rendered content with action buttons -->
                        $records                        
                    </tr>
                {% endfor %}        
                </tbody>
            </table>
        </div>
    </div>
    {% else %}
    <div class="row">
        <div class="col-12">

            <div class="alert alert-info alert-dismissible fade show" role="alert">
               <h5>No record exists for selected entity</h5>
              <p>You may create new record using the ADD command </p>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="col">                    
               <ul class="list-inline">
                  <li class="list-inline-item">
                    <a class="btn btn-primary" 
                    href="{% url '$model_create_url' %}" >Add</a>
                  </li>
                  <li class="list-inline-item">
                    <a class="btn btn-primary" 
                        href="{{ request.META.HTTP_REFERER }}">
                        Back
                    </a>
                  </li>                      
                </ul>                    
            </div>          
        </div>        
    </div>
    </div>
    {% endif %}
    {% endblock %}
"""
)

paginated_list_template = Template(
"""
{% for object in page_obj %}            
    {{ object }}            
{% endfor %}

<div class="pagination">
    <span class="step-links">
        {% if page_obj.has_previous %}
            <a href="?page=1">&laquo; first</a>
            <a href="?page={{ page_obj.previous_page_number }}">previous</a>
        {% endif %}

        <span class="current">
            Page {{ page_obj.number }} of {{ page_obj.paginator.num_pages }}.
        </span>

        {% if page_obj.has_next %}
            <a href="?page={{ page_obj.next_page_number }}">next</a>
            <a href="?page={{ page_obj.paginator.num_pages }}">last &raquo;</a>
        {% endif %}
    </span>
</div>
"""
)

detail_template = Template(
"""
{% extends "base.html" %}
{% load static %}
{% block content %}
    
    <div class="container my-5">
        <div class="row">
            <div class="col">
                <a class="btn btn-primary"
                   href="{% url '$model_list_url' %}">View all
                </a>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">$model Details</h5>
                    </div>
                    <div class="card-body">
                        $details
                    </div>
                </div>
            </div>
        </div>
    
        <div class="row mt-4">
            <div class="col p-3">
            <ul class="list-inline">
            <li class="list-inline-item">
                <a class="btn btn-primary" 
                    href="{% url '$model_update_url' object.slug %}">Edit
                </a>
            </li>
    
            <li class="list-inline-item">
                <a class="btn btn-primary"
                   href="{% url '$model_delete_url' object.slug %}">Delete
                </a>
            </li>
    
            </ul>            
            </div>
        </div>
    </div>
    {% endblock %}
"""
)

instance_form_template = Template(
"""
    {% extends "base.html" %}
    {% load static %}
    {% load crispy_forms_tags %}
    {% block content %}
    <div class="container my-5">
    <p>
        <a class="btn btn-primary" href="{% url '$model_list_url' %}">$model Listing</a>
    </p>
    <form method="post">
    {% csrf_token %}
    {{form|crispy}}
    <button class="btn btn-primary" type="submit">Submit</button>
    </form>
    </div>
    {% endblock %}
"""
)

confirm_delete_template = Template(
"""
    {% extends "base.html" %}
    {% load static %}
    {% load crispy_forms_tags %}
    {% block content %}
    <div class="container my-5">
    <form method="post">{% csrf_token %}
        <h5>Delete $model</h5>
        <p>Are you sure you want to delete {{ object }}?</p>
        <div class="form-group">
            <ul class="list-inline">                
                <li class="list-inline-item">
                    <a href="{% url '$model_list_url' %}" class="btn btn-primary">Cancel</a>
                </li>
                <li class="list-inline-item">
                    <button type="submit" class="btn btn-danger">Delete</button>            
                </li>                
            </ul>
        </div>
    </form>
    </div>
    {% endblock %}
"""
)
