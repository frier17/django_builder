import pathlib2
from setuptools import setup

HERE = pathlib2.Path(__file__).parent

README = (HERE / "README.md").read_text()

setup(name='cli-django-builder',
      version='0.1.0',
      description='A command line interface to build Django application and RESTful application using DRF',
      url='https://gitlab.com/frier17/cli_django_builder.git',
      author='https://gitlab.com/frier17/',
      author_email='frier17@a17s.co.uk',
      long_description=README,
      long_description_content_type="text/markdown",
      license="MIT",
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Intended Audience :: Developers',
          'Topic :: Software Development :: Build Tools',

          "License :: OSI Approved :: MIT License",
          "Programming Language :: Python :: 3",
          "Programming Language :: Python :: 3.8",
      ],
      packages=['django_builder', 'django_builder/management'],
      include_package_data=True
      )
